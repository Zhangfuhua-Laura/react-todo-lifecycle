import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

const App = () => {
  return (
    <div className='App'>
      <TodoList/>
    </div>
  );
};

export default App;